package context

import "context"

type KeyReference string

const (
	UserIdKey     = KeyReference("UserId")
	ResourceIdKey = KeyReference("ResourceId")
	TokenKey      = KeyReference("TokenKey")
	AccountIdKey  = KeyReference("AccountId")
	OfficeIdKey   = KeyReference("OfficeId")
	ServiceIdKey  = KeyReference("ServiceId")
	UserNameKey   = KeyReference("UserName")
)

type Values struct {
	UserId     string
	UserName   string
	ResourceId string
	Token      string
	AccountId  string
	OfficeId   string
	ServiceId  string
}

func ContextKeys(ctx context.Context) (data Values) {
	if ctx == nil {
		return
	}

	if rawUserId, ok := ctx.Value(UserIdKey).(string); ok {
		data.UserId = rawUserId
	}

	if rawUserName, ok := ctx.Value(UserNameKey).(string); ok {
		data.UserName = rawUserName
	}

	if rawResourceId, ok := ctx.Value(ResourceIdKey).(string); ok {
		data.ResourceId = rawResourceId
	}

	if rawToken, ok := ctx.Value(TokenKey).(string); ok {
		data.Token = rawToken
	}

	if rawAccountId, ok := ctx.Value(AccountIdKey).(string); ok {
		data.AccountId = rawAccountId
	}

	if rawOfficeId, ok := ctx.Value(OfficeIdKey).(string); ok {
		data.OfficeId = rawOfficeId
	}

	if rawServiceId, ok := ctx.Value(ServiceIdKey).(string); ok {
		data.ServiceId = rawServiceId
	}

	return
}
