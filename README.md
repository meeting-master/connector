# connector

data source connections

# packages

* context
* es (elastic search)
* pgsql

# Installation 

`go get -u gitlab.com/meeting-master/connector`

# Import

`import gitlab.com/meeting-master/connector/pgsql`